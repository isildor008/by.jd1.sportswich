package model;

import java.util.Arrays;

/**
 * Created by isild on 05.04.2017.
 */
public class Manager {


    private UnitedModel[] freeUnits = new UnitedModel[10];
    Client client = new Client();
    InicializationArray inicializationArray = new InicializationArray();
    RentUnit rentUnit = inicializationArray.inicialization();

    public UnitedModel[] addInArray(UnitedModel newElement) {

        freeUnits = addRentUnit(freeUnits, newElement);

        return freeUnits;
    }

    private UnitedModel[] addRentUnit(UnitedModel[] units, UnitedModel equipment) {
        units = Arrays.copyOf(units, (units.length * 3) / 2 + 1);
        units[units.length - 1] = equipment;
        return units;
    }

    public UnitedModel[] getFree() {

        for (int i = 0; i < rentUnit.getUnits().length; i++) {

            if (rentUnit.getStatus(i) != null && rentUnit.getStatus(i)==true) {
                addInArray(rentUnit.getUnits(i));

            }

        }
        return freeUnits;
    }


    public void getByCategory() {
        getFree();
        for (int i = 0; i < freeUnits.length; i++) {

            if (freeUnits[i] != null && freeUnits[i].getCategory().equals(Category.SPRING)) {


            }
        }


    }

    public Client getRentUnitByClient(int idClient) {

        if(client.getId()==idClient){
        client.addInArray(getByIdUnit(5));
            rentUnit.setStatus(false,5);
        client.addInArray(getByIdUnit(3));
            rentUnit.setStatus(false,3);
            return client;
        }

        return null;
    }

    public UnitedModel getByIdUnit(int id){
        int i;
        for ( i=0 ; i < freeUnits.length; i++) {
            if (freeUnits[i] != null&& freeUnits[i].getId()==id) {
                return freeUnits[i];

            }
        }

        return null;
    }






    @Override
    public String toString() {
        return "Manager{" +
                "freeUnits=" + Arrays.toString(freeUnits) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Manager manager = (Manager) o;
        return Arrays.equals(freeUnits, manager.freeUnits);

    }

    @Override
    public int hashCode() {
        return freeUnits != null ? Arrays.hashCode(freeUnits) : 0;
    }
}
