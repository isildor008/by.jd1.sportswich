package model;


import model.sportswich.Equipment;
import util.IdGenerator;

import java.util.Arrays;

/**
 * Created by isild on 02.04.2017.
 */
public class Client {
    private int id= IdGenerator.getId();
    private String name;
    private UnitedModel rent[] = new Equipment[3];
    private int timeStart;
    private int timeEnd;

    public Client() {

    }

    public Client(int id, String name, UnitedModel[] rent, int timeStart, int timeEnd) {
        this.id = id;
        this.name = name;
        this.rent = rent;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
    }

    public UnitedModel[] addInArray(UnitedModel newElement) {

        rent = addRentUnit(rent, newElement);

        return rent;
    }

    private UnitedModel[] addRentUnit(UnitedModel[] units, UnitedModel equipment) {
        units = Arrays.copyOf(units, (units.length * 3) / 2 + 1);
        units[units.length - 1] = equipment;
        return units;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UnitedModel[] getRent() {
        return rent;
    }

    public void setRent(UnitedModel[] rent) {
        this.rent = rent;
    }

    public int getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(int timeStart) {
        this.timeStart = timeStart;
    }

    public int getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(int timeEnd) {
        this.timeEnd = timeEnd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;

        if (id != client.id) return false;
        if (timeStart != client.timeStart) return false;
        if (timeEnd != client.timeEnd) return false;
        if (name != null ? !name.equals(client.name) : client.name != null) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(rent, client.rent);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (rent != null ? Arrays.hashCode(rent) : 0);
        result = 31 * result + timeStart;
        result = 31 * result + timeEnd;
        return result;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", rent=" + Arrays.toString(rent) +
                ", timeStart=" + timeStart +
                ", timeEnd=" + timeEnd +
                '}';
    }
}
