package model;


import util.IdGenerator;

public class UnitedModel {

    private int id = IdGenerator.getId();
    private  String color;
    private boolean sex;
    private String Manufacturer;
    private Category category;
    private boolean status;


    public UnitedModel(){

    }

    public UnitedModel(int id, String color, boolean sex, String manufacturer, Category category, boolean status) {
        this.id = id;
        this.color = color;
        this.sex = sex;
        Manufacturer = manufacturer;
        this.category = category;
        this.status = status;
    }

    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public String getManufacturer() {
        return Manufacturer;
    }


    public void setManufacturer(String manufacturer) {
        Manufacturer = manufacturer;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UnitedModel that = (UnitedModel) o;

        if (id != that.id) return false;
        if (sex != that.sex) return false;
        if (status != that.status) return false;
        if (color != null ? !color.equals(that.color) : that.color != null) return false;
        if (Manufacturer != null ? !Manufacturer.equals(that.Manufacturer) : that.Manufacturer != null) return false;
        return category == that.category;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (color != null ? color.hashCode() : 0);
        result = 31 * result + (sex ? 1 : 0);
        result = 31 * result + (Manufacturer != null ? Manufacturer.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (status ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UnitedModel{" +
                "id=" + id +
                ", color='" + color + '\'' +
                ", sex=" + sex +
                ", Manufacturer='" + Manufacturer + '\'' +
                ", category=" + category +
                ", status=" + status +
                '}';
    }
}
