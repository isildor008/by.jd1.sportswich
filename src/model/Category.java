package model;

/**
 * Created by isild on 01.04.2017.
 */
public enum Category {
    SUMMER("summer"),
    WINTER("winter"),
    SPRING("spring"),
    AUTUMN("autumn");

    private String text;

    Category(String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }

    public static Category fromString(String text) {
        for (Category category : Category.values()) {
            if (category.text.equalsIgnoreCase(text)) {
                return category;
            }
        }
        return null;
    }

}
