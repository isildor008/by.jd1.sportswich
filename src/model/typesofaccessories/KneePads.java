package model.typesofaccessories;

/**
 * Created by isild on 02.04.2017.
 */
public class KneePads extends Accessories {

    private String fixing;

    public KneePads(){

    }

    public KneePads(String fixing) {
        this.fixing = fixing;
    }

    public String getFixing() {
        return fixing;
    }

    public void setFixing(String fixing) {
        this.fixing = fixing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KneePads kneePads = (KneePads) o;

        return !(fixing != null ? !fixing.equals(kneePads.fixing) : kneePads.fixing != null);

    }

    @Override
    public int hashCode() {
        return fixing != null ? fixing.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "KneePads{" +
                "fixing='" + fixing + '\'' +
                '}';
    }
}
