package model.typesofaccessories;

/**
 * Created by isild on 02.04.2017.
 */
public class Helmet extends Accessories {

    private String form;

    private double weight;

    public Helmet(){

    }

    public Helmet(String form, double weight) {
        this.form = form;
        this.weight = weight;
    }

    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Helmet helmet = (Helmet) o;

        if (Double.compare(helmet.weight, weight) != 0) return false;
        return !(form != null ? !form.equals(helmet.form) : helmet.form != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = form != null ? form.hashCode() : 0;
        temp = Double.doubleToLongBits(weight);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Helmet{" +
                "form='" + form + '\'' +
                ", weight=" + weight +
                '}';
    }
}
