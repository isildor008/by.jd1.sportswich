package model.typesofaccessories;

import model.UnitedModel;

/**
 * Created by isild on 01.04.2017.
 */
public abstract class Accessories extends UnitedModel {
    private int size;

    private String material;

    public Accessories(){

    }

    public Accessories(int size, String material) {
        this.size = size;
        this.material = material;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Accessories that = (Accessories) o;

        if (size != that.size) return false;
        return !(material != null ? !material.equals(that.material) : that.material != null);

    }

    @Override
    public int hashCode() {
        int result = size;
        result = 31 * result + (material != null ? material.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Accessories{" +
                "size=" + size +
                ", material='" + material + '\'' +
                '}';
    }
}
