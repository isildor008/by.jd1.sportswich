package model.typesofaccessories;

/**
 * Created by isild on 02.04.2017.
 */
public class Gloves extends Accessories {
    private int lenght;
    private String type;

    public Gloves(){

    }

    public Gloves(int lenght, String type) {
        this.lenght = lenght;
        this.type = type;
    }

    public int getLenght() {
        return lenght;
    }

    public void setLenght(int lenght) {
        this.lenght = lenght;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Gloves gloves = (Gloves) o;

        if (lenght != gloves.lenght) return false;
        return !(type != null ? !type.equals(gloves.type) : gloves.type != null);

    }

    @Override
    public int hashCode() {
        int result = lenght;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Gloves{" +
                "lenght=" + lenght +
                ", type='" + type + '\'' +
                '}';
    }


}
