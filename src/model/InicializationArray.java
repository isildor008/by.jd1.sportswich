package model;

import model.sportswich.*;
import model.typesofaccessories.Gloves;
import model.typesofaccessories.Helmet;

/**
 * Created by isild on 02.04.2017.
 */
public class InicializationArray {

    RentUnit rentUnit = new RentUnit();

    public RentUnit inicialization() {
        new Bike();
        UnitedModel bike = new Bike(3, 27);
        bike.setColor("red");
        bike.setManufacturer("Stels");
        bike.setCategory(Category.SUMMER);
        bike.setSex(true);
        bike.setStatus(true);
        rentUnit.addInArray(bike);


        UnitedModel rollerSkates = new RollerSkates(40, 4);
        rollerSkates.setColor("blue");
        rollerSkates.setManufacturer("River");
        rollerSkates.setCategory(Category.SPRING);
        rollerSkates.setSex(false);
        rollerSkates.setStatus(true);
        rentUnit.addInArray(rollerSkates);


        UnitedModel skates = new Skates(38, true);
        skates.setColor("white");
        skates.setManufacturer("Tor");
        skates.setSex(true);
        skates.setStatus(true);
        skates.setCategory(Category.WINTER);
        skates.setSex(true);
        rentUnit.addInArray(skates);


        UnitedModel skiing = new Skiing(45, 200, "policorbonat");
        skiing.setColor("black");
        skiing.setSex(false);
        skiing.setStatus(true);
        skiing.setCategory(Category.WINTER);
        skiing.setManufacturer("Gear");
        rentUnit.addInArray(skiing);


        UnitedModel gloves = new Gloves(30, "Skiings");
        gloves.setColor("white");
        gloves.setSex(true);
        gloves.setCategory(Category.WINTER);
        gloves.setManufacturer("enum");


        Helmet helmet = new Helmet("Sport", 525);
        helmet.setColor("red");
        helmet.setSex(true);
        helmet.setCategory(Category.SUMMER);
        helmet.setManufacturer("Viking");
        helmet.setSize(12);

        return rentUnit;
    }

}
