package model;

import java.util.Arrays;


/**
 * Created by isild on 01.04.2017.
 */
public class RentUnit {

    private UnitedModel units[] = new UnitedModel [10];


    public UnitedModel[] addInArray(UnitedModel newElement) {

        units = addRentUnit(units, newElement);

        return units;
    }

    private UnitedModel[] addRentUnit(UnitedModel[] units, UnitedModel equipment) {
        units = Arrays.copyOf(units, (units.length * 3) / 2 + 1);
        units[units.length - 1] = equipment;
        return units;
    }

    public Boolean getStatus(int i){
        if(units[i] == null)return null;
        return units[i].isStatus();
    }

    public void setStatus(boolean status,int id){
        for(int i=0 ;i<units.length;i++){

            if(units[i] != null &&units[i].getId()==id){
                units[i].setStatus(status);}

        }



    }

    @Override
    public String toString() {
        return  "RentUnit{" +
                "units=" + Arrays.toString(units) +
                '}';
    }

    public UnitedModel[] getUnits() {
        return units;
    }

    public UnitedModel getUnits(int i) {
        return units[i];
    }

    public void setUnits(UnitedModel[] units) {
        this.units = units;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RentUnit rentUnit = (RentUnit) o;

        return Arrays.equals(units, rentUnit.units);

    }

    @Override
    public int hashCode() {
        return units != null ? Arrays.hashCode(units) : 0;
    }
}
