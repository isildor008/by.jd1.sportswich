package model.sportswich;


import model.UnitedModel;

/**
 * Created by isild on 01.04.2017.
 */
public abstract   class Equipment extends UnitedModel {


 private int year;

    public Equipment(){

    }

    public Equipment(int year) {
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Equipment equipment = (Equipment) o;

        return year == equipment.year;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + year;
        return result;
    }

    @Override
    public String toString() {
        return "Equipment{" +
                "year=" + year +
                '}';
    }
}
