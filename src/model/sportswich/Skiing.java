package model.sportswich;

/**
 * Created by isild on 01.04.2017.
 */
public class Skiing extends Equipment{

    private int Size;

    private int LongSkiing;

    private String Covering;

    public Skiing(){

    }

    public Skiing(int size, int longSkiing, String covering) {
        Size = size;
        LongSkiing = longSkiing;
        Covering = covering;
    }

    public int getSize() {
        return Size;
    }

    public void setSize(int size) {
        Size = size;
    }

    public int getLongSkiing() {
        return LongSkiing;
    }

    public void setLongSkiing(int longSkiing) {
        LongSkiing = longSkiing;
    }

    public String getCovering() {
        return Covering;
    }

    public void setCovering(String covering) {
        Covering = covering;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Skiing skiing = (Skiing) o;

        if (Size != skiing.Size) return false;
        if (LongSkiing != skiing.LongSkiing) return false;
        return !(Covering != null ? !Covering.equals(skiing.Covering) : skiing.Covering != null);

    }

    @Override
    public int hashCode() {
        int result = Size;
        result = 31 * result + LongSkiing;
        result = 31 * result + (Covering != null ? Covering.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Skiing{" +
                "Size=" + Size +
                ", LongSkiing=" + LongSkiing +
                ", Covering='" + Covering + '\'' +
                '}';
    }
}
