package model.sportswich;

/**
 * Created by isild on 01.04.2017.
 */
public class Skates extends Equipment {

    private int Size;

    private boolean Sharpen;

    public Skates(){

    }

    public Skates(int size, boolean sharpen) {
        Size = size;
        Sharpen = sharpen;
    }

    public int getSize() {
        return Size;
    }

    public void setSize(int size) {
        Size = size;
    }

    public boolean isSharpen() {
        return Sharpen;
    }

    public void setSharpen(boolean sharpen) {
        Sharpen = sharpen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Skates skates = (Skates) o;

        if (Size != skates.Size) return false;
        return Sharpen == skates.Sharpen;

    }

    @Override
    public int hashCode() {
        int result = Size;
        result = 31 * result + (Sharpen ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Skates{" +
                "Size=" + Size +
                ", Sharpen=" + Sharpen +
                '}';
    }
}
