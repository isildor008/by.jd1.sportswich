package model.sportswich;

/**
 * Created by isild on 01.04.2017.
 */
public class RollerSkates extends Equipment{

    private int Size;

    private int CountWheel;

    public RollerSkates(){

    }

    public RollerSkates(int size, int countWheel) {
        Size = size;
        CountWheel = countWheel;
    }

    public int getSize() {
        return Size;
    }

    public void setSize(int size) {
        Size = size;
    }

    public int getCountWheel() {
        return CountWheel;
    }

    public void setCountWheel(int countWheel) {
        CountWheel = countWheel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RollerSkates that = (RollerSkates) o;

        if (Size != that.Size) return false;
        return CountWheel == that.CountWheel;

    }

    @Override
    public int hashCode() {
        int result = Size;
        result = 31 * result + CountWheel;
        return result;
    }

    @Override
    public String toString() {
        return "RollerSkates{" +
                "Size=" + Size +
                ", CountWheel=" + CountWheel +
                '}';
    }
}
