package model.sportswich;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by isild on 01.04.2017.
 */
@XmlRootElement
public class  Bike extends Equipment {
    private int Frame;
    private int DiameterWheel;

    public Bike(){

    }

    public Bike(int frame, int diameterWheel) {
        Frame = frame;
        DiameterWheel = diameterWheel;
    }

    public Bike getBikeFromFile(){


        return new Bike();
    }


    public int getFrame() {
        return Frame;
    }
    @XmlElement
    public void setFrame(int frame) {
        Frame = frame;
    }

    public int getDiameterWheel() {
        return DiameterWheel;
    }
    @XmlElement
    public void setDiameterWheel(int diameterWheel) {
        DiameterWheel = diameterWheel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Bike bike = (Bike) o;

        if (Frame != bike.Frame) return false;
        return DiameterWheel == bike.DiameterWheel;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Frame;
        result = 31 * result + DiameterWheel;
        return result;

    }

    @Override
    public String toString() {
        return "Bike{" +
                "Frame=" + Frame +
                ", DiameterWheel=" + DiameterWheel +
                "Sex=" + isSex() +
                ", Color='" + getColor() + '\'' +
                ", Manufacturer='" + getManufacturer() + '\'' +
                '}';
    }
}
