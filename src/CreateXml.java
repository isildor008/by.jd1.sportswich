import model.Category;
import model.UnitedModel;
import model.sportswich.Bike;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import java.io.StringWriter;

/**
 * Created by isild on 28.04.2017.
 */
public class CreateXml {
    public static void main(String[] args) {
        Bike bike =new Bike();
        bike.setCategory(Category.AUTUMN);
        bike.setManufacturer("gdgd");
        bike.setSex(true);
        bike.setId(4);
        bike.setColor("red");
        bike.setStatus(true);

        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(Bike.class);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        Marshaller jaxbMarshaller = null;
        try {
            jaxbMarshaller = jaxbContext.createMarshaller();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        try {
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        } catch (PropertyException e) {
            e.printStackTrace();
        }
        StringWriter sw = new StringWriter();
        try {
            jaxbMarshaller.marshal(bike,sw);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        String xmlString = sw.toString();
        System.out.println(xmlString);
    }
}
