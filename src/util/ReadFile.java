package util;

import model.Category;
import model.UnitedModel;
import model.sportswich.Bike;
import model.sportswich.RollerSkates;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by isild on 16.04.2017.
 */
public class ReadFile {

    public static final String FILENAME = "E:\\JavaCourse\\by.jd1.sportswich\\src\\model\\util\\rentequpment.txt";
    List<String[]> array=new ArrayList<>();
  Bike bike=new Bike();
    RollerSkates rollerSkates=new RollerSkates();

    public List<String[]> read(){


        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(FILENAME))) {
            StringBuilder stringBuilder = new StringBuilder();
            String line = bufferedReader.readLine();


            while (line != null) {
                String[] parts= line.split(",");
                stringBuilder.append(parts);
                stringBuilder.append(System.lineSeparator());
                line = bufferedReader.readLine();
                array.add(parts);
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return array;
    }

    public UnitedModel parsByObject(){

        for(int i=0;i<array.size();i++){

        switch (array.get(i)[0]) {
            case ("bike"): array.get(i)[0].equals("bike");

                bike.setId(Integer.parseInt(array.get(i)[1]));
                bike.setFrame(Integer.parseInt(array.get(i)[2]));
                bike.setYear(Integer.parseInt(array.get(i)[3]));
                bike.setColor(array.get(i)[4]);
                bike.setSex(Boolean.parseBoolean(array.get(i)[5]));
                bike.setManufacturer(array.get(i)[6]);
                bike.setCategory(Category.fromString(array.get(i)[7]));
                bike.setStatus(Boolean.parseBoolean(array.get(i)[8]));
                break;

            case ("rollerSkates"): array.get(i)[0].equals("rollerSkates");
                rollerSkates.setId(Integer.parseInt(array.get(i)[1]));
                rollerSkates.setSize(Integer.parseInt(array.get(i)[2]));
                rollerSkates.setCountWheel(Integer.parseInt(array.get(i)[3]));
                rollerSkates.setYear(Integer.parseInt(array.get(i)[4]));
                rollerSkates.setColor(array.get(i)[5]);
                rollerSkates.setSex(Boolean.parseBoolean(array.get(i)[6]));
                rollerSkates.setManufacturer(array.get(i)[7]);
                rollerSkates.setCategory(Category.fromString(array.get(i)[8]));
                rollerSkates.setStatus(Boolean.parseBoolean(array.get(i)[9]));
                break;


        }}
        return new UnitedModel();

    }

}
