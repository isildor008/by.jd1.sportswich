package util;

/**
 * Created by isild on 10.04.2017.
 */
public class IdGenerator {
    private static int counter = 1;

    private IdGenerator() {
    }
    public static synchronized int getId(){
        return counter++;
    }
}
